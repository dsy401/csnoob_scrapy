import scrapy

class uoa_cs_staff_spider(scrapy.Spider):
    name = 'uoa_staff'
    start_urls = ['https://www.cs.auckland.ac.nz/people/search#?search=&page=1&rows=20&category=staff&orderBy=relevance']

    def parse(self, response):
        infos = response.xpath('//*[@id="search_list"]').extract()
        print(response.xpath('//*[@id="search_list"]/div[2]').extract())
        for i in range(1,len(infos)+1):
            print(response.xpath('//*[@id="search_list"]/div[' + str(i) + ']/div[2]/h1/a/text()').extract())
            # print(response.xpath('//*[@id="search_list"]/div[' + str(i) + ']').extract())
