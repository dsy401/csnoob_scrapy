import scrapy
from AKL_csnoob_spiders.items import cs_course_item
class uoa_cs_course_spider(scrapy.Spider):
    name="uoa"
    allowed_domains=['www.calendar.auckland.ac.nz']
    start_urls = ['https://www.calendar.auckland.ac.nz/en/courses/faculty-of-science/computer-science.html?_ga=2.49029414.1667703311.1562937860-294829308.1533108480#COMPSCI_110Introduction_to_Computer_Systems']


    def parse(self,response):
        infos = response.xpath('//*[@id="regs"]/div[2]/div[@class="courseStage section"]').extract()
        for i in range(1,len(infos)+1):
            index = str(i+1)
            courses = response.xpath('//*[@id="regs"]/div[2]/div['+index+']/div[1]/div').extract()
            for j in range(1,len(courses)+1):
                Item = cs_course_item()
                code = response.xpath('//*[@id="regs"]/div[2]/div['+index+']/div[1]/div['+str(j)+']/div[@class="subject"]/div[@class="courseA"]/text()').extract()[1].strip()
                Item['code'] = code
                Item['name'] = response.xpath('//*[@id="regs"]/div[2]/div['+index+']/div[1]/div['+str(j)+']/p[@class="title"]/text()').extract_first().strip() ##处理None Type
                Item['description'] = response.xpath('//*[@id="regs"]/div[2]/div['+index+']/div[1]/div['+str(j)+']/p[@class="description"]/text()').extract_first().strip()
                Item['school'] = 1
                Item['type'] = self.type_check(code)
                yield Item

    def type_check(self,code):
        position = code.find(" ")
        stage = int(code[position+1])
        if stage > 3:
            return 1
        return 0




