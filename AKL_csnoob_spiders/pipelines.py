# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import pymongo
import ssl
class AklCsnoobSpidersPipeline(object):

    def __init__(self):
        self.client = pymongo.MongoClient("mongodb+srv://dsy401:dyfi6yth@csnoob-jouvh.mongodb.net/test?retryWrites=true&w=majority",
                             ssl_cert_reqs=ssl.CERT_NONE)

    def process_item(self, item, spider):
        if spider.name == 'uoa':
            try:
                db = self.client.csnoob
                course = db.course
                course.insert_one({
                    'code': item['code'],
                    'name': item['name'],
                    'description': item['description'],
                    'school': item['school'],
                    'type': item['type']
                })
            except Exception as e:
                print(e)
            return item

