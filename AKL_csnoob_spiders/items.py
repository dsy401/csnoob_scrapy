# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class cs_course_item(scrapy.Item):
    code = scrapy.Field()
    name = scrapy.Field()
    description = scrapy.Field()
    school = scrapy.Field()
    type = scrapy.Field()  # 0 is undergraduate, 1 is postgraduate
